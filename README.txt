Create, update and delete products

Create
POST to /AvenueCode/rest/product
Example: {"name":"Small TV","description":"nao assitir", "parent": 2}

Update
PUT to /AvenueCode/rest/product/{product id}
Example: {"name":"Mini TV 3"}

Delete
DELETE to /AvenueCode/rest/product/{product id}
no body is necessary

Create, update and delete images

Create
POST to rest/image/product/{product id}
Example: {"type": "image type"}

Update
PUT to /AvenueCode/rest/image/{image id}
Example: {"type": "image type 2"}

Delete
DELETE to /AvenueCode/rest/image/{image id}

Get all products excluding relationships (child products, images) 
GET to /AvenueCode/rest/product/light
Return example:
[
  {
"description": "to listen",
"id": 1,
"name": "Radio"
},
  {
"description": "to watch",
"id": 2,
"name": "TV"
},
  {
"description": "to watch",
"id": 3,
"name": "Small TV"
}
],

Get all products including specified relationships (child product and/or images) 
GET to /AvenueCode/rest/product/full
it's not correctly returning the child content


The operations bellow has not been implemented yet
Same as 3 using specific product identity 
Same as 4 using specific product identity 
Get set of child products for specific product 
Get set of images for specific product


The project was created using Hibernate, Maven and H2 embedded database to store data. Tables are created by the application.
No maven tests was created.

Add this project to your server (eg: Tomcat), then run the server and call the URLS provided above.