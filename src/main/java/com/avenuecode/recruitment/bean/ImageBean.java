package com.avenuecode.recruitment.bean;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class ImageBean {
	private Long id;
	private String type;
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	@Override
	public String toString() {
		return "ImageBean [id=" + id + ", type=" + type + "]";
	}
}
