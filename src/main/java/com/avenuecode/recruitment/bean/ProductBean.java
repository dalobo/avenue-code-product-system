package com.avenuecode.recruitment.bean;

import javax.xml.bind.annotation.XmlRootElement;

import com.avenuecode.recruitment.hibernate.Product;

@XmlRootElement   
public class ProductBean  {
    private Long id;
    private String name;
    private String description;
    private Long parent;
    
    public ProductBean() {}
    
    public ProductBean(Product product) {
    		this.id = product.getId();
    		this.name = product.getName();
    		this.description = product.getDescription();
    		this.parent = product.getParent() != null ? product.getParent().getId() : null;
    }
    
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public Long getParent() {
		return parent;
	}
	public void setParent(Long parent) {
		this.parent = parent;
	}
	@Override
	public String toString() {
		return "ProductBean [id=" + id + ", name=" + name + ", description=" + description + ", parent=" + parent + "]";
	}
	
}