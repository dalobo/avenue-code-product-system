package com.avenuecode.recruitment.hibernate;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

@Entity
public class Image {
	@Id
    @GeneratedValue
    @Column(name = "image_id", unique = true, nullable = false)
	private long id;
	
	private String type;
	
	@ManyToOne(fetch = FetchType.EAGER, 
            cascade = { CascadeType.ALL})
    @JoinColumn(name = "product_id")
	private Product product;
	
	public Image() {}
	
	public Image(String type) {
		this.type = type;
	}
	
	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public Product getProduct() {
		return product;
	}

	public void setProduct(Product product) {
		this.product = product;
	}

	@Override
	public String toString() {
		return "Image [id=" + id + ", type=" + type + "]";
	}
	
}
