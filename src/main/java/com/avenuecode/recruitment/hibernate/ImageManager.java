package com.avenuecode.recruitment.hibernate;

import org.hibernate.Session;
import org.hibernate.SessionFactory;

import com.avenuecode.recruitment.bean.ImageBean;
import com.avenuecode.recruitment.hibernate.util.MySessionFactory;

public class ImageManager extends Manager{
	
	public void create(ImageBean bean, long productId) {
		SessionFactory sessionFactory = MySessionFactory.getSessionFactory();
		Session session = sessionFactory.openSession();
		
		Product product =  (Product) session.load(Product.class, productId);
		
		Image image = new Image();
		image.setType(bean.getType());
		image.setProduct(product);
		
	    session.beginTransaction();
	    session.save(image);
	    session.getTransaction().commit();
	}
	
	public void update(ImageBean bean, Long imageId) {
		SessionFactory sessionFactory = MySessionFactory.getSessionFactory();
		Session session = sessionFactory.openSession();
		
		Image image =  (Image) session.load(Image.class, imageId);
		
		
		if(bean.getType() != null) {
			image.setType(bean.getType());
		}
		
		session.beginTransaction();
	    session.update(image);
	    session.getTransaction().commit();
	}
	
	public void delete(Long id) {
		SessionFactory sessionFactory = MySessionFactory.getSessionFactory();
		
		Image image = new Image();
		image.setId(id);
		
	    Session session = sessionFactory.openSession();
	    session.beginTransaction();
	    session.delete(image);
	    session.getTransaction().commit();
	}
}
