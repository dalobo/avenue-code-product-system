package com.avenuecode.recruitment.hibernate;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;

import com.avenuecode.recruitment.hibernate.util.MySessionFactory;

public class Manager {
	public void showAll() {
		SessionFactory sessionFactory = MySessionFactory.getSessionFactory();
        Session session = sessionFactory.openSession();
        @SuppressWarnings("unchecked")
        List<Product> products = session.createQuery("FROM Product").list();
        products.forEach((x) -> System.out.printf("- %s%n", x));
        session.close();
        
    		sessionFactory = MySessionFactory.getSessionFactory();
        session = sessionFactory.openSession();
        @SuppressWarnings("unchecked")
        List<Image> images = session.createQuery("FROM Image").list();
        images.forEach((x) -> System.out.printf("- %s%n", x));
        session.close();
	}
}
