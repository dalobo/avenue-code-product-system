package com.avenuecode.recruitment.hibernate;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.persistence.*;

@Entity
public class Product {
	
	@Id
    @GeneratedValue
    @Column(name = "product_id", unique = true, nullable = false)
	private long id;
	
	private String name;
	
	private String description;
	
	@ManyToOne(cascade={CascadeType.ALL})
	@JoinColumn(name="parent_id")
	private Product parent;

	
	@OneToMany(mappedBy = "product", fetch = FetchType.EAGER)
	private List<Image> images = new ArrayList<Image>();
	
	public Product() {
		
	}
	
	public Product(String name, String description, Product parentProduct, List<Image> images) {
		super();
		this.name = name;
		this.description = description;
		this.parent = parentProduct;
		this.images = images;
	}
	
	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Product getParent() {
		return parent;
	}

	public void setParent(Product parent) {
		this.parent = parent;
	}
	
	public List<Image> getImages() {
		return images;
	}

	public void setImages(List<Image> images) {
		this.images = images;
	}
	
	public void addImage(Image image) {
		this.images.add(image);
	}

	@Override
	public String toString() {
		return "Product [id=" + id + ", name=" + name + ", description=" + description + ", parent=" + parent
				+ "]";
	}
}
