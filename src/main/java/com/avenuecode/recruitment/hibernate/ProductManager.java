package com.avenuecode.recruitment.hibernate;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;

import com.avenuecode.recruitment.bean.ProductBean;
import com.avenuecode.recruitment.hibernate.util.MySessionFactory;
import com.avenuecode.recruitment.rest.response.FullProduct;
import com.avenuecode.recruitment.rest.response.LightProduct;

public class ProductManager extends Manager{
	
	public void create(ProductBean bean) {
		SessionFactory sessionFactory = MySessionFactory.getSessionFactory();
		Session session = sessionFactory.openSession();
		
		Product parent = null;
		if(bean.getParent() != null) {
			parent =  (Product) session.load(Product.class, bean.getParent());
		}
		
		Product product = new Product(bean.getName(), bean.getDescription(), parent, null);
		
	   
	    session.beginTransaction();
	    session.save(product);
	    session.getTransaction().commit();
	}
	
	public void update(ProductBean bean, Long id) {
		SessionFactory sessionFactory = MySessionFactory.getSessionFactory();
		Session session = sessionFactory.openSession();
		
		Product product =  (Product) session.load(Product.class, id);
		
		if(bean.getParent() != null) {
			Product parent =  (Product) session.load(Product.class, bean.getParent());
			product.setParent(parent);
		}
		
		if(bean.getName() != null) {
			product.setName(bean.getName());
		}
		
		if(bean.getDescription() != null) {
			product.setDescription(bean.getDescription());
		}
		
		session.beginTransaction();
	    session.update(product);
	    session.getTransaction().commit();
	}
	
	public void delete(Long id) {
		SessionFactory sessionFactory = MySessionFactory.getSessionFactory();
		
		Product product = new Product();
		product.setId(id);
		
	    Session session = sessionFactory.openSession();
	    session.beginTransaction();
	    session.delete(product);
	    session.getTransaction().commit();
	}
	
	public List<LightProduct> loadAllLight() {
		SessionFactory sessionFactory = MySessionFactory.getSessionFactory();
        Session session = sessionFactory.openSession();
        @SuppressWarnings("unchecked")
        List<Product> products = session.createQuery("FROM Product").list();
        
        session.close();
        
        List<LightProduct> lightProducts = new ArrayList<LightProduct>();
        
        products.forEach((x) -> lightProducts.add(new LightProduct(x)));
        
        return lightProducts;
	}
	
	public List<FullProduct> loadAllFull() {
		SessionFactory sessionFactory = MySessionFactory.getSessionFactory();
        Session session = sessionFactory.openSession();
        @SuppressWarnings("unchecked")
        List<Product> products = session.createQuery("FROM Product").list();
        
        session.close();
        
        List<FullProduct> fullProducts = new ArrayList<FullProduct>();
        
        products.forEach((x) -> fullProducts.add(new FullProduct(x)));
        
        return fullProducts;
	}
}