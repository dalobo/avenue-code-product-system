package com.avenuecode.recruitment.hibernate.util;

import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

public class MySessionFactory {
	private final static SessionFactory sessionFactory = new Configuration().configure()
            .buildSessionFactory();
	
	public static SessionFactory getSessionFactory() {
		return sessionFactory;
	}
}
