package com.avenuecode.recruitment.rest;

import javax.ws.rs.*;
import javax.ws.rs.core.Response;

import org.codehaus.jettison.json.JSONException;

import com.avenuecode.recruitment.bean.ImageBean;
import com.avenuecode.recruitment.hibernate.ImageManager;

@Path("/image")
public class ImageService {
	ImageManager imageManager = new ImageManager();
	
	@Path("/product/{productId}")
	@POST
	public Response createImage(ImageBean image, @PathParam("productId") Long productId) throws JSONException {
		imageManager.create(image, productId);

		System.out.print("Saved" + image);
		
		imageManager.showAll();
		
		return Response.status(200).build();
	}
	
	@PUT
	@Path("/{id}")
	public Response updateImage(ImageBean image, @PathParam("productId") Long productId, @PathParam("id") Long imageId) throws JSONException {
		imageManager.update(image, imageId);

		System.out.print("Updated" + image);
		
		imageManager.showAll();
		
		return Response.status(200).build();
	}
	
	@DELETE
	@Path("/{id}")
	public Response deleteImage(@PathParam("id") Long imageId) throws JSONException {
		imageManager.delete(imageId);

		System.out.print("Deleted" + imageId);
		
		imageManager.showAll();
		
		return Response.status(200).build();
	}
}
