package com.avenuecode.recruitment.rest;

import java.util.List;

import javax.ws.rs.*;
import javax.ws.rs.core.Response;

import org.codehaus.jettison.json.JSONException;

import com.avenuecode.recruitment.bean.ProductBean;
import com.avenuecode.recruitment.hibernate.ProductManager;
import com.avenuecode.recruitment.rest.response.FullProduct;
import com.avenuecode.recruitment.rest.response.LightProduct;


@Path("/product")
public class ProductService {
	ProductManager productManager = new ProductManager();
	
	@GET
	@Path("/full")
	public Response getAllProductsFull() throws JSONException {
		List<FullProduct> fullProduct = productManager.loadAllFull();
		return Response.ok(fullProduct).build();
	}
	
	@GET
	@Path("/light")
	public Response getAllProductsLight() throws JSONException {
		List<LightProduct> lightProduct = productManager.loadAllLight();
		return Response.ok(lightProduct).build();
	}
	
	@POST
	public Response createProduct(ProductBean product) throws JSONException {
		productManager.create(product);

		System.out.print("Saved" + product);
		
		productManager.showAll();
		
		return Response.status(200).build();
	}
	
	@PUT
	@Path("/{id}")
	public Response updateProduct( ProductBean product, @PathParam("id") Long id) throws JSONException {
		productManager.update(product, id);

		System.out.print("Updated" + product);
		
		productManager.showAll();
		
		return Response.status(200).build();
	}
	
	@DELETE
	@Path("/{id}")
	public Response deleteProduct(@PathParam("id") Long id) throws JSONException {
		productManager.delete(id);

		System.out.print("Deleted" + id);
		
		productManager.showAll();
		
		return Response.status(200).build();
	}
}