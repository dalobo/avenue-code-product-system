package com.avenuecode.recruitment.rest.response;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlRootElement;

import com.avenuecode.recruitment.hibernate.Image;
import com.avenuecode.recruitment.hibernate.Product;

@XmlRootElement   
public class FullProduct  {
    private Long id;
    private String name;
    private String description;
    private FullProduct parent;
    private List<LightImage> images;
    
    public FullProduct() {}
    
    public FullProduct(Product product) {
    		this.id = product.getId();
    		this.name = product.getName();
    		this.description = product.getDescription();
    		
    		if(product.getParent() != null) {
    			this.parent = new FullProduct(product.getParent());
    		}
    		
    		List<Image> images = product.getImages();
    		
    		if(images != null) {
    			this.images = new ArrayList<LightImage>();
    			images.forEach((x) -> this.images.add(new LightImage(x)));
    		}
    }
    
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}

	@Override
	public String toString() {
		return "FullProduct [id=" + id + ", name=" + name + ", description=" + description + ", parent=" + parent
				+ ", images=" + images + "]";
	}	
}