package com.avenuecode.recruitment.rest.response;


import com.avenuecode.recruitment.hibernate.Image;

public class LightImage {
	private Long id;
	private String type;
	
	public LightImage() {}
	
	public LightImage(Image image) {
		this.id = image.getId();
		this.type = image.getType();
	}
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	@Override
	public String toString() {
		return "LightImage [id=" + id + ", type=" + type + "]";
	}
}
