package com.avenuecode.recruitment.rest.response;

import javax.xml.bind.annotation.XmlRootElement;

import com.avenuecode.recruitment.hibernate.Product;

@XmlRootElement   
public class LightProduct  {
    private Long id;
    private String name;
    private String description;
    
    public LightProduct() {}
    
    public LightProduct(Product product) {
    		this.id = product.getId();
    		this.name = product.getName();
    		this.description = product.getDescription();
    }
    
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}

	@Override
	public String toString() {
		return "LightProduct [id=" + id + ", name=" + name + ", description=" + description + "]";
	}
	
}